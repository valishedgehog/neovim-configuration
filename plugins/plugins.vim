"
" vim-plug pluggin manager
" https://github.com/junegunn/vim-plug
"

if &compatible
  set nocompatible
endif

filetype off

call plug#begin()
    " Start of plugins list

    " Tree files navigation
    Plug 'scrooloose/nerdtree'

    " Comments
    Plug 'scrooloose/nerdcommenter'

    " Display the indention levels with thin vertical line
    Plug 'Yggdroot/indentLine'

    " Neovim themes pack
    Plug 'flazz/vim-colorschemes'

    " Highlight trailing whitespace
    Plug 'bronson/vim-trailing-whitespace'

    " Insert and delete brackets, parens, quotes in pair
    Plug 'jiangmiao/auto-pairs'

    " Syntax checking for vim
    Plug 'scrooloose/syntastic'

    " Vim Session
    Plug 'xolox/vim-misc'
    Plug 'xolox/vim-session'

    " Airline and themes
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    " Language pack for neovim
    Plug 'sheerun/vim-polyglot'

    " Tagbar displays the tags of the current file in a sidebar
    Plug 'majutsushi/tagbar'

    " Gitgutter
    Plug 'airblade/vim-gitgutter'

    " Multiple Cursors
    Plug 'terryma/vim-multiple-cursors'

    " End of plugins list
call plug#end()

filetype plugin indent on
syntax enable

" Load plugin settings
for f in glob('~/.config/nvim/plugins/config/*.vim', 0, 1)
    execute 'source' f
endfor

