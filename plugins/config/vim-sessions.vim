let g:session_directory = "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1
let g:session_autosave_periodic = 5
let g:session_autosave_silent = 1
