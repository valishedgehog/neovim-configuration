# Neovim configuration

Install neovim on Ubuntu
```shell
sudo apt install neovim exuberant-ctags -y
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

## TODO
- [ ] Mappings for tabs
- [ ] Cheatsheats for plugins
