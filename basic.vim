language en_US.UTF-8

set showmatch
set number
set showcmd
set cursorline
set wildmenu
set lazyredraw
set confirm
set hidden
set mousehide
set title
set history=500
set timeoutlen=500
set autoread
set noshowmode

" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary
set ttyfast

" Searching
set magic
set hlsearch
set ignorecase
set smartcase
set nohlsearch
set incsearch

" Indention
set nowrap
set autoindent
set smartindent
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set colorcolumn=180

" Backups
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
set writebackup

" Folding
set foldenable
set foldmethod=indent
set foldlevel=100

