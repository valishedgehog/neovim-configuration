autocmd StdinReadPre * let s:std_in=1
autocmd FocusGained,BufEnter * :silent! !
autocmd FocusLost,WinLeave * :silent! noautocmd w

" Clean extra spaces on save
autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()

" Visual mode pressing * or # searches for the current selection
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>
