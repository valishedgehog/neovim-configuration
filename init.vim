execute 'source' '~/.config/nvim/basic.vim'
execute 'source' '~/.config/nvim/plugins/plugins.vim'
execute 'source' '~/.config/nvim/map.vim'
execute 'source' '~/.config/nvim/functions.vim'
execute 'source' '~/.config/nvim/commands.vim'

" Reload config if some files were changed changed
autocmd! BufWritePost ~/.config/nvim/*.vim mapclear | source $MYVIMRC | redraw | echo "Vim configuration reloaded"

" Color scheme
colorscheme badwolf
